import java.util.*;

import java.util.Scanner;
//ROUND ROBIN SCHEDULING - By Kathia Teran

public class ArraySorting {
	 public static void main(String args[])
	 {  
	    
		 int num = 15;
		 System.out.println("\nRound Robin CPU Scheduler - by Kathia Teran");
		 System.out.println("-------------------------------------------");
		 System.out.println("\nThis test is for [" + num + "] Processes:");
		  
	    //ARRIVALS
	    int[] arrivals = new int[num];
	    //Queue 1
	    Queue<Integer> queue1 = new LinkedList<Integer>();
	    
	    for(int i = 1; i < arrivals.length; i++)
	    {
	    	arrivals[i] = (int)(Math.random()*num + 1);	     
	    }  
	   // Arrays.sort(arrivals);       
	    Arrays.sort(arrivals);    
      
	    for(int i = 0; i < arrivals.length; i++) {
	    		queue1.add(arrivals[i]);
	    }
	    
	    System.out.println("\nThe Arrival Times are: " + queue1);      
        
      
        //BURST times --> max of 6
        System.out.println("\nBursts, max of 6 /[P]");
	    int maxBurst = 6;
	   
	    int[] bursts = new int[num];            
	    
	    for(int i = 0; i < bursts.length; i++)
	    {
	      bursts[i] = (int)(Math.random()*maxBurst + 1);
	    } 
	    
	    //Sorts & prints the bursts in array
	    Arrays.sort(bursts);      
        System.out.println("Order of bursts /[P]:   " + Arrays.toString(bursts));       	         
  	 
	//QUANTUM
    Scanner userInput = new Scanner(System.in);

    System.out.print("\nEnter a Quantum no more than 6: ");
 	   int quantum = userInput.nextInt();
	    

	 //PROCESS TIME Array
 	  int[] processTime = new int[num];   
 	    
	    for(int i = 0; i <processTime.length; i++)
	    {
	    	processTime[i] = bursts[i]-quantum;
	    } 	   
	    // System.out.println("The Process times are: " + Arrays.toString(processTime));   
   		    
	   //QUEUE PROCESSING	   	   
	   Queue<Integer> queue2 = new LinkedList<Integer>();
	   Queue<Integer> queue3 = new LinkedList<Integer>();
	   Queue<Integer> queue4 = new LinkedList<Integer>();
	  
		 
		//queue2	 - Print First
		for(int i=0; i<num; i++) { 		
			 
				if (processTime[i]==0 || processTime[i]<0) {
					int pDone = queue1.remove();
					pDone = 0;
					int printFirst = pDone+arrivals[i]+bursts[i];					
					queue2.add(printFirst);					
					}				
		}	
		
		//queue3 - prints 2nd			
				for(int i=0; i<num; i++) { 	
					if (processTime[i]>0 && processTime[i]<=quantum && arrivals[i]!=num) {
						int pDone = queue1.remove();
						pDone = 0;
						int PT = processTime[i];
						int arr = arrivals[i];//+
						int add2A = num - arr;//+
						int printNext = pDone+arr+quantum+PT+add2A;																				
						queue3.add(printNext);
						}	
			}	
				
		//queue4 - prints last
				for(int i=0; i<num; i++) { 	
					int PT2 = processTime[i]	- quantum;
					if(PT2>0 && PT2<=quantum && arrivals[i]!=num ) {
						int pDone = queue1.remove();
						pDone = 0;
						int arr = arrivals[i];//+
						int add2A = num - arr;//+
						int printNext2 = pDone+arr+quantum+PT2+add2A;																				
						queue4.add(printNext2);	
					}			
				}	
		
		//System.out.println("\n" + queue2 + queue3 + queue4 + "\n");
	
	//Conversion of queues to arrays
		Integer [] array1 = null;
		Integer [] array2 = null;
		Integer [] array3 = null;
		
		array1 = queue2.toArray(new Integer[queue2.size()]);
		array2 = queue3.toArray(new Integer[queue3.size()]);	
		array3 = queue4.toArray(new Integer[queue4.size()]);
		
		
		//THIS IS HOW IT PRINTS		
		System.out.println("\nOrder of Process Service Time /[P] \n(before final completion time):\n" );
			
//Arrays with completions
		System.out.print("                      [");
		for(int i=0; i<array1.length; i++) {
			int show = array1[i];
			System.out.print(" "+show +" ");
		}
		
		for(int i=0; i<array2.length; i++) {
			int show2 = array2[i];
			System.out.print(" "+show2 +" ");
		}
		
		for(int i=0; i<array3.length; i++) {
			int show3 = array3[i];
			System.out.print(" "+show3 +" ");
		}
		System.out.print("]");
		//REPEATED ELEMENTS in each array

		 //array1		 
		 int count1 = 1;
	         for (int i = 0; i < array1.length; i++) {

	            for (int j = i +1 ; j < array1.length; j++) {
	                if (array1[i] == array1[j]) {
	                    count1++;
	                }
	            }
	            if (count1 > 1) {
	                i = i + count1 - 1;
	            }
	         }//System.out.println("\n"+count1);
 
		 
	      //array2			 
			 int count2 = 1;
		         for (int i = 0; i < array2.length; i++) {

		            for (int j = i +1 ; j < array2.length; j++) {
		                if (array2[i] == array2[j]) {
		                    count2++;
		                }
		            }
		            if (count2 > 1) {		             
		                i = i + count2 - 1;
		            }
		        }//System.out.println(count2);
	 
		   //array3			 
			int count3 = 1;
			       for (int i = 0; i < array3.length; i++) {

			          for (int j = i +1 ; j < array3.length; j++) {
			              if (array3[i] == array3[j]) {
			                   count3++;
			              }
			          }
			            if (count3 > 1) {
			                i = i + count3 - 1;
			            }
			        } 
			       
	//Largest Process Times in each array      
			       //array2
			        int max1 = Integer.MIN_VALUE;
			        for (int x : array2) {
			            if (array2.length > 0 && x > max1 && x>0) {
			                max1 = x;
			            }
			        }
			        		if(max1<0)
			        		{
			        			System.out.print("");
			        		}
			       
			        	//array3
			        int max2 = Integer.MIN_VALUE;			       
			        for (int x : array3) {
			            if (array3.length> 0 && x > max2 && x>0) {
			                max2 = x;
			            }
			        }
			        
			       if(max2<0)
			        {
			        		System.out.print("");
			        }					
System.out.println("\n");    
// COMPLETION TIME 			       
		int allCounts= count1 + count2 + count3;
		System.out.println("The total added Wait Time is: "+ allCounts + "\n");
		
					       
		int maxValue = 0;
		int maxValue2 = 0;
		
			if(max2>max1) {
				System.out.println("\nThe Average Turnaround Time is: "+ max2 + "\n");
				maxValue=max2;
				int CompletionTime = maxValue + allCounts;				
				System.out.println("The Final Completion Time is: "+ CompletionTime);
			}
			
			else 
				System.out.println("\nThe Average Turnaround Time is: "+ max1 + "\n");
				maxValue2=max1;
				int CompletionTime2 = maxValue2 + allCounts;
				System.out.println("\nThe Final Completion Time is: "+ CompletionTime2);
			
			
			System.out.println("---------------------------------");
			
				
		
	   userInput.close();
	 }	 
}	 
	   
